import Vue from 'vue'
import Pusher from 'pusher-js'

Vue.use({
  install(Vue) {
    Vue.prototype.$pusher = new Pusher(process.env.VUE_APP_PUSHER_KEY, { cluster: "ap1" })
  }
})
