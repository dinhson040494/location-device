export const ENDPOINT = {
  /* Authe & autho */
  API_LOGIN: 'api/auth/login',
  API_REGISTER: 'api/auth/sign-up',
  API_LOGOUT: 'api/auth/logout',
  PROFILE: 'api/users/me',


  /* Master data */
  TRANSACTION: 'api/transaction',
  TRANSACTION_CONTENT: 'api/assist/transaction-content',
  CREATE_QR: 'api/assist/create-qr',
  SYSTEM_STATUS: 'api/system-status',
  CANCEL_QR: 'api/assist/transaction-cancel',
  APPROVED_BY_HAND: 'api/assist/transaction-approved',

  /* Admin */
  ACCOUNTS: 'api/admin/users',
  SHOPS: 'api/admin/shops',
  CREATE_ACCOUNT: 'api/admin/create-user',
  BANK_INFO: 'api/admin/bank-info',
  LOCK_ACCOUNT: 'api/admin/user/deactive',
  RESET_PASSWORD_ACCOUNT: 'api/admin/user/reset-password',

  /* Change password */
  CHANGE_PASSWORD: 'api/user/update-password',
}
