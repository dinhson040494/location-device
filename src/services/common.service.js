import BaseService from './base.service'
import { ENDPOINT } from '~/api/endpoint'

class CommonService extends BaseService {
  constructor(option) {
    super(option)
    return this
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  register(payload) {
    return this.http.post(ENDPOINT.API_REGISTER, payload).then(res => res)
  }
  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  login(payload) {
    return this.http.post(ENDPOINT.API_LOGIN, payload).then(res => res)
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  logout() {
    return this.http.get(ENDPOINT.API_LOGOUT).then(res => res)
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  getProfile() {
    return this.http.get(ENDPOINT.PROFILE).then(res => res.data)
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  getTransactions(params) {
    return this.http.get(ENDPOINT.TRANSACTION, { params }).then(res => res.data)
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  getUsers(params) {
    return this.http.get(ENDPOINT.ACCOUNTS, { params }).then(res => res.data)
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  getShops(params) {
    return this.http.get(ENDPOINT.SHOPS, { params }).then(res => res.data)
  }

  /**
   * @description
   * [Danh sách] - Danh sách role
   */
  getTransactionContent() {
    return this.http.get(ENDPOINT.TRANSACTION_CONTENT).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async createQR(payload) {
    return this.http.post(ENDPOINT.CREATE_QR, payload).then(res => res.data)
  }

  /**
   * Get system status
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async getSystemStatus() {
    return this.http.get(ENDPOINT.SYSTEM_STATUS).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async cancelQR(payload) {
    return this.http.put(ENDPOINT.CANCEL_QR, payload).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async onApproveByHand(payload) {
    return this.http.put(ENDPOINT.APPROVED_BY_HAND, payload).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async createAccount(payload) {
    return this.http.post(ENDPOINT.CREATE_ACCOUNT, payload).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async changePassword(payload) {
    return this.http.put(ENDPOINT.CHANGE_PASSWORD, payload).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async updateBankInfo(payload) {
    return this.http.put(ENDPOINT.BANK_INFO, payload).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async lockAccount(payload) {
    return this.http.put(ENDPOINT.LOCK_ACCOUNT, payload).then(res => res.data)
  }

  /**
   * Creates a QR code.
   * @returns {Promise<any>} A promise that resolves with the QR code data.
   */
  async resetPassword(payload) {
    return this.http.put(ENDPOINT.RESET_PASSWORD_ACCOUNT, payload).then(res => res.data)
  }
}

export default new CommonService({ auth: true })
