import Vue from 'vue'
import services from '@/services'

Vue.use({
  install(Vue) {
    Vue.prototype.$services = services
  }
})
