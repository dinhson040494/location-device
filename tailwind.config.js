module.exports = {
  purge: ["./public/**/*.html", "./src/**/*.vue"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      "SanFrancisco": ["SanFrancisco"],
      "SanFranciscoBold": ["SanFrancisco-Bold"],
      "Mabella": ["Mabella"],
    },
    extend: {
      colors: {
        primary: '#FBB040',
        yellow: {
          50: "#FBB040",
        },
        green: {
          50: "#4CAF50",
        },
        danger: "#FC1414",
        grey: {
          50: '#f9f9f9',
          150: '#4B4646'
        },
        'color-1': '#C6C4C1',
        'color-2': '#474747',
        'color-3': '#E8E6E6',
        'color-4': '#4B4646',
        'color-5': '#4F79E7'
      },
      width: {
        '69/16': '4.313rem',
        '352/16': '22rem',
        '400/16': '25rem',
        '443/16': '27.688rem',
        50: "#f9f9f9",
        150: "#4B4646",
      },
      height: {
        "67/16": "4.188rem",
      },
      maxWidth: {
        '160/16': '10rem',
        '192/16': '12rem',
        '230/16': '14.375rem',
        '80/16': '5rem'
      },
      maxHeight: {
        '128': '32rem',
      },
      minWidth: {
        "64/16": "4rem",
        '160/16': '10rem',
        '320/16': '20rem',
        "352/16": "22rem",
        "416/16": "26rem",
        "500/16": "31.25rem",
      },
      borderRadius: {
        "10/16": "0.625rem",
      },
      gridTemplateColumns: {
        "120px/1fr": "120px 1fr"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
