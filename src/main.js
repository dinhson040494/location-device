import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import { EVENT } from './utils/constants'
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';
import 'vuetify/dist/vuetify.min.css'
import '@/assets/css/tailwind.css'
import '@/assets/css/index.scss'
import './plugins/vue-toast'
import './plugins/services'
import './plugins/pusher'
import './plugins/modal'

Vue.config.productionTip = false

const eventBus = new Vue()
Vue.prototype.$eventBus = eventBus
Vue.prototype.$loading = {
  start() {
    eventBus.$emit(EVENT.LOADING, true)
  },
  finish() {
    eventBus.$emit(EVENT.LOADING, false)
  }
}
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
