import AppModal from '~/components/AppModal.vue'
import Vue from 'vue'
Vue.component('app-modal', AppModal)

Vue.use({
  install(Vue) {

    Vue.prototype.$modal = {
      show(params) {
        Vue.prototype.$eventBus.$emit('show-modal', params)
      }
    }
  }
})
