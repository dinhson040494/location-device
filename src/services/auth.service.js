import { Http } from "./http.init";
import { ResponseWrapper, ErrorWrapper } from "./util";
import $store from "~/store";
import $router from "~/router";
import { COOKIE_TOKEN_EXPIRE } from "~/utils/constants";
import cookies from "~/utils/cookie";
import CommonService from "./common.service";

export class AuthService {
  /**
   ******************************
   * @API
   ******************************
   */
  static async register({ phone, password, name }) {
    try {
      const requestData = {
        Phone: phone,
        Password: password,
        Name: name,
      };
      const response = await CommonService.register(requestData);
      return new ResponseWrapper(response, response.data);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  static async makeLogin({ phone, password }) {
    try {
      const requestData = {
        phone,
        password,
      };
      const response = await CommonService.login(requestData);
      _setAuthData({
        accessToken: response.data.token,
        exp: _parseTokenData(response.data.token).exp,
      });
      return new ResponseWrapper(response, response.data);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  static async makeLogout() {
    try {
      const response = await CommonService.logout();
      _resetAuthData();
      $router.push({ name: "Login" }).catch(() => {});
      return new ResponseWrapper(response, response.data.data);
    } catch (error) {
      throw new ErrorWrapper(error);
    }
  }

  /**
   ******************************
   * @METHODS
   ******************************
   */
  static debounceRefreshTokens() {
    const token = cookies.get("token");
    return Promise.resolve({
      data: {
        accessToken: token,
      },
    });
  }

  static isAccessTokenExpired() {
    const accessTokenExpDate = $store.state.accessTokenExpDate - 10;
    const nowTime = Math.floor(new Date().getTime() / 1000);

    return accessTokenExpDate <= nowTime;
  }

  static hasRefreshToken() {
    return Boolean(localStorage.getItem("refreshToken"));
  }

  static setRefreshToken(status) {
    if (!["", "true"].includes(status)) {
      throw new Error(
        `setRefreshToken: invalid value ${status}; Expect one of ['', 'true']`
      );
    }

    localStorage.setItem("refreshToken", status);
  }

  static getBearer() {
    const token = cookies.get("token");
    return `Bearer ${token}`;
  }

  static setBearer(accessToken) {
    const expires = new Date(Date.now() + COOKIE_TOKEN_EXPIRE);
    const cookieOption = {
      path: "/",
      expires,
    };
    cookies.set("token", accessToken, cookieOption);
  }

  static resetAuthData() {
    _resetAuthData();
  }
}

/**
 ******************************
 * @private_methods
 ******************************
 */

// eslint-disable-next-line no-unused-vars
function _parseTokenData(accessToken) {
  let payload = "";
  let tokenData = {};

  try {
    payload = accessToken.split(".")[1];
    tokenData = JSON.parse(atob(payload));
  } catch (error) {
    throw new Error(error);
  }

  return tokenData;
}

function _resetAuthData() {
  // reset userData in store
  $store.commit("setData", { accessTokenExpDate: null });
  // reset tokens
  AuthService.setRefreshToken("");
  AuthService.setBearer("");
  cookies.remove("token");
}

// eslint-disable-next-line no-unused-vars
function _setAuthData({ accessToken, exp } = {}) {
  AuthService.setRefreshToken("true");
  AuthService.setBearer(accessToken);
  $store.commit("setData", { accessTokenExpDate: exp });
}
