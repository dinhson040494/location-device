export const generateItems = (count, creator) => {
  const result = [];
  for (let i = 1; i <= count; i++) {
    result.push(creator(i));
  }
  return result;
}

export const applyDrag = (arr, dragResult) => {
  const { removedIndex, addedIndex, payload } = dragResult;
  if (removedIndex === null && addedIndex === null) return arr;

  const result = [...arr];
  let itemToAdd = payload;

  if (removedIndex !== null) {
    itemToAdd = result.splice(removedIndex, 1)[0];
  }

  if (addedIndex !== null) {
    result.splice(addedIndex, 0, itemToAdd);
  }

  return result;
};

export const formatVnd = (num) => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' vnđ';
}

export const assignObjWithExistKey = (resource, destination) => {
  Object.keys(resource).forEach((key) => {
    if (destination[key] !== undefined) {
      destination[key] = resource[key];
    }
  });
}

export const formatDate = (date) => {
  if (typeof date === "string" && date) {
    const [year, month, day] = date.split('-');
    return `${day.padStart(2, 0)}-${month.padStart(2, 0)}-${year}`
  }
  return null;
}

export const parseDate = (date) => {
  if (typeof date === "string" && date) {
    const [day, month, year] = date.split('-');
    return `${year}-${month.padStart(2, 0)}-${day.padStart(2, 0)}`
  }
  return null;
}

export const debounce = (fn, delay) => {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn(...args);
    }, delay);
  };
};

export const randomNumber = (length) => {
  return Math.floor(Math.random() * length)
}

export const nonAccentVietnamese = (str) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  str = str.toLowerCase();
  return str
}

export const formatTime = (isoDateString) => {
  // Convert to Date object
  const date = new Date(isoDateString);

  // Convert to Vietnam time (GMT+7) with Vietnamese locale
  const options = {
    timeZone: 'Asia/Ho_Chi_Minh', // Vietnam's timezone
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false
  };

  const dateInVietnam = date.toLocaleString('vi-VN', options);
  const formattedDate = dateInVietnam.replace(/(\d{2})\/(\d{2})\/(\d{4}), (\d{2}:\d{2}:\d{2})/, '$1/$2/$3 $4');
  return formattedDate;
}

export const getDate = (isoDateString) => {
  // Convert to Date object
  const date = new Date(isoDateString);

  var year = date.toLocaleString("default", { year: "numeric" });
  var month = date.toLocaleString("default", { month: "2-digit" });
  var day = date.toLocaleString("default", { day: "2-digit" });
  return day + "-" + month + "-" + year;
}

export const getTime = (isoDateString) => {
  // Convert to Date object
  const date = new Date(isoDateString); const options = {
    timeZone: 'Asia/Ho_Chi_Minh', // Vietnam's timezone
  
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false
  };

  const dateInVietnam = date.toLocaleString('vi-VN', options);
  return dateInVietnam;
}

export const moneyFormat = (moneyString) => {
  var moneyString = moneyString.split(".");
  moneyString[0]=moneyString[0].replace(/\B(?=(\d{3})+(?!\d))/g,".");
  return moneyString.join(",");
}

